 $msdeploy = "c:\Program Files\IIS\Microsoft Web Deploy V3\msdeploy.exe"

function packageContent {

    Param(
        [string]$contentPath,
        [string]$destinationPackage,
        [string]$parametersFile
    )

    $source = "-source:contentPath=" + $contentPath
    $dest = "-dest:package=" + $destinationPackage
    $declaredParams = "-declareparamfile:" + $parametersFile

    $msdeployArgs = @(
            $source,
            $dest,
            $declaredParams,
            "-verb:sync",
            "-verbose"
            )

    & $msdeploy $msdeployArgs

}

function packageIIS {

    Param(
        [string]$manifest,
        [string]$destinationPackage,
        [string]$parametersFile
    )

    $source = "-source:manifest=" + $manifest
    $dest = "-dest:package=" + $destinationPackage
    $declaredParams = "-declareparamfile:" + $parametersFile

    $msdeployArgs = @(
            $source,
            $dest,
            $declaredParams,
            "-verb:sync",
            "-disableLink:ContentExtension",
            "-verbose"
            )

    & $msdeploy $msdeployArgs
}

function deployIIS {

    param(
        [string]$package,
        [string]$computerName,
        [string]$setParametersFile
    )

    $source =  "-source:package=" + $package
    $dest = "-dest:auto,computerName=" + $computerName
    $setParams = "-setParamFile:" + $setParametersFile

    $msdeployArgs = @(
            $source,
            $dest,
            $setParams,
            "-verb:sync",
            "-verbose"
            )

    & $msdeploy $msdeployArgs

}

function deployContent {

    Param(
        [string]$package,
        [string]$targetFolder,
        [string]$setParametersFile
    )

    $source =  "-source:package=" + $package
    $dest =  "-dest:contentPath=" + $targetFolder
    $setParams = "-setParamFile:" + $setParametersFile

    $msdeployArgs = @(
            $source,
            $dest,
            $setParams,
            "-verb:sync",
            "-verbose"
            )

    & $msdeploy $msdeployArgs
    
}

