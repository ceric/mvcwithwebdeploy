param ([string]$env)

. .\environments.ps1
. .\webDeploy.ps1

function generateParameters {

    param([string]$env)

    $parametersTemplate = Get-Content "$PSScriptRoot\setparameters.template.xml"

    foreach ($keyValue in $environmentConfig.$env.GetEnumerator()) {
        $parametersTemplate = $parametersTemplate.replace('${' + $keyValue.Name + '}', $keyValue.Value)
    }

    Write-Host "Generating setParameters.$env.xml as:" 
    Write-Host $parametersTemplate

    $setParametersFile = "setparameters.$env.xml"
    Write-Host $setParametersFile
    If (Test-Path $setParametersFile){
        Remove-Item $setParametersFile
    }

    $parametersTemplate | out-file -filepath $setParametersFile
}

function packageIISConfig {

    packageIIS `
        "manifest.xml" `
        $environmentConfig.iisPackage `
        "parameters.xml"
}

function buildContent {

    param([string]$env)

    $msbuild = "c:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe"
    $outputPath =  "/p:OutputPath=" + $environmentConfig.buildOutputPath

    & $msbuild @(
        $environmentConfig.webProjFile,
        "/t:rebuild",
        "/p:Configuration=Release",
        $outputPath)

}

function packageWeb {

    packageContent `
        $environmentConfig.buildContentOutputPath `
        $environmentConfig.contentPackage `
        'parameters.xml'
}

function deployIISConfig {

    param([string]$env)

    deployIIS `
        $environmentConfig.iisPackage `
        "127.0.0.1" `
        "setparameters.$env.xml"

}

function deployWebContent {

    param([string]$env)

    deployContent `
        $environmentConfig.contentPackage `
        $environmentConfig.$env.contentPath `
        "setparameters.$env.xml"
}

#generate setparameters file for content path:
generateParameters($env)

# Write-Host "building Content" -foregroundcolor White -backgroundcolor DarkRed
# buildContent($env)
# Write-Host "packaging Content" -foregroundcolor White -backgroundcolor DarkRed
# packageWeb
# Write-Host "deploying Content" -foregroundcolor White -backgroundcolor DarkRed
# deployWebContent($env)

# Write-Host "packaging IIS" -foregroundcolor White -backgroundcolor DarkRed
# packageIISConfig($env)
# Write-Host "deploying IIS" -foregroundcolor White -backgroundcolor DarkRed
# deployIISConfig($env)

#deploy content
