@echo off


IF "%~1"=="" goto setDefaultEnvironment
IF NOT "%~1"=="" goto setEnvironmentFromCommandLine


:setDefaultEnvironment
SET DEPLOYMENT_ENV=dev
goto deploy

:setEnvironmentFromCommandLine
SET DEPLOYMENT_ENV="%~1"
goto deploy


:deploy

powershell -command ".\deployAll.ps1 -env %DEPLOYMENT_ENV%; exit $LASTEXITCODE"

