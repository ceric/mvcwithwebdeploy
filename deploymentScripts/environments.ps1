$parentDir = (get-item $PSScriptRoot).parent.FullName

$environmentConfig = @{

        buildOutputPath = "$parentDir\deploymentArtifacts\releaseBuild";
        buildContentOutputPath = "$parentDir\deploymentArtifacts\releaseBuild\_PublishedWebsites\BasicMvcFive.Web";
        webProjFile = "$parentDir\BasicMvcFive.Web\BasicMvcFive.Web.csproj";
        iisPackage = "$parentDir\deploymentArtifacts\iispackage.zip";
        contentPackage = "$parentDir\deploymentArtifacts\webContent.zip";

        dev = @{
            
            newRelicAppName = 'BlahBlah';
            contentPath = 'c:\testDeployment';
            }; 
        qa = @{
             newRelicAppName = 'BlahBlah';
            contentPath = 'c:\testDeployment';
            };
        live = @{
            newRelicAppName = 'BlahBlah';
            contentPath = 'c:\testDeployment';
            };
    }
