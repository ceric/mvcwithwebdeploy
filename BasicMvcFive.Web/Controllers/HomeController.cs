﻿using System.Web.Mvc;

namespace BasicMvcFive.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}